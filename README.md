# Semantic layer data preparation

This repository describes the post-processing of semantic layer data derived from
the Sphaera project database.

The post-processing includes time-slicing the data for various possible historical
assumptions, as well as the preparation of the data for visualization in
the [Muxviz](http://muxviz.net/) multilayer visualization tool.

## Raw data

The raw data for edges and book information is published separately as

> Victoria Beyer, Nana Citron, Manon Gumpert, Gesa Funke, Irina Tautschnig, Malte Vogl, Christoph Sander, Florian Kräutli, Matteo Valleriani (2020). Sphaera -- Book and Edge Data, version 22-05-2019. DARIAH-DE. https://doi.org/10.20375/0000-000c-d8d5-0

The notebooks are directly loading data from the published repository.

## Communities

The resulting book communities are available as HTML and markdown tables in the `communities` folder. To view tables on Gitlab, use the markdown fiels (`.md`).

## Interactive notebooks

An interactive version of the Notebooks can be accessed via the GESIS Binder:

For the post-processing of the raw edge data:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2FMPIWG%2FDepartment-I%2Fsphaera%2Fsphaera-extended-sem-data.git/b71c94ac2ff59a2472ca638fb49a51b8e43d5cb0?filepath=1_Create_semantic_layers.ipynb)
File: 1_Create_semantic_layers.ipynb

For the creation of Muxviz formatted data and the time slices:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2FMPIWG%2FDepartment-I%2Fsphaera%2Fsphaera-extended-sem-data.git/b71c94ac2ff59a2472ca638fb49a51b8e43d5cb0?filepath=2_Time_and_influence_batching.ipynb)
File: 2_Time_and_influence_batching.ipynb
